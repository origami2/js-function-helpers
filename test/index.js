var lib = require('..');
var assert = require('assert');

describe('.getFunctionName', function () {
  it('gets function name', function () {
    assert.equal(
      'fn1',
      lib.getFunctionName(
        function fn1() {}
      )
    );
  });
});

describe('.renameFunction', function () {
  var renamed;
  var date = Date.now();

  before(function () {

    renamed = lib.renameFunction(
      'fn2',
      function fn1() {return date;}
    );
  });

  it('renames the function', function () {
    assert.equal(
      'fn2',
      renamed.name
    );
  });

  it('works the same', function () {
    assert.equal(
      date,
      renamed()
    );
  });
});

describe('.getFunctionArgumentNames', function () {
  it('gets function name', function () {
    assert.deepEqual(
      ['a', 'b', 'c'],
      lib.getFunctionArgumentNames(
        function fn1(a, b, c) {}
      )
    );
  });
});

describe('.createMethod', function () {
  var newMethod;
  var date;

  var oldMethod = function (a, b) {
    return a + b + date;
  };

  before(function () {
    date = Date.now();
  });

  it('creates a new method', function () {
    newMethod = lib.createMethod(
      'fn3',
      ['a', 'b'],
      oldMethod
    );

    assert.equal('function', typeof(newMethod));
    assert.equal('fn3', newMethod.name);
    assert.deepEqual(
      ['a', 'b'],
      lib.getFunctionArgumentNames(newMethod)
    );
  });

  it('works just the same', function () {
    assert.equal(
      date + 1 + 2,
      newMethod(1, 2)
    );
    assert.equal(
      date,
      newMethod(0, 0)
    );
  })
});

describe('.getFunctionBody', function () {
  it('gets function body', function () {
    assert.deepEqual(
      'return arg1;',
      lib.getFunctionBody(
        function fn1(arg1) {return arg1;}
      )
    );
  });
});

describe('.createApi', function () {
  var date;

  before(function () {
    date = Date.now();
  });

  it('returns a function', function () {
    assert.equal(
      'function',
      typeof(
        lib.createApi(
          'MyAPI',
          {},
          function (apiName, methodName) {
          }
        )
      )
    );
  });

  it('invokes methodBuilder', function (done) {
    lib
    .createApi(
      'MyAPI',
      {
        'method1': ['arg1', 'arg2']
      },
      function (apiName, methodName, argNames) {
        assert.equal('MyAPI', apiName);
        assert.equal('method1', methodName);
        assert.deepEqual(['arg1', 'arg2'], argNames);

        done();
      }
    );
  });

  it('methods works', function () {
    var API = lib.createApi(
      'MyAPI',
      {
        'method1': ['arg1', 'arg2']
      },
      function (apiName, methodName, argNames) {
        return function (arg1, arg2) {
          return [apiName, methodName, argNames, arg1, arg2, date];
        }
      }
    );
    var api = new API();

    assert.deepEqual(
      ['MyAPI', 'method1', ['arg1', 'arg2'], 1, 2, date],
      api.method1(1, 2)
    );
  });
});
