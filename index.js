var FN_ARGS = /^(function)?\s*[^\(]*\(\s*([^\)]*)\)/m;
var FN_ARG_SPLIT = /,/;
var FN_ARG = /^\s*(_?)(.+?)\1\s*$/;
var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;

function renameFunction (name, fn, argNames) {
  return (new Function("return function (call) { return function " + name +
      " (" + (argNames || getFunctionArgumentNames(fn)).join(',') +") { return call(this, arguments) }; };")())(Function.apply.bind(fn));
};

function ShadowMethod(methodName, argNames, implementation) {
  var method = function () {
    var args = [];

    for (var i = 0; i < arguments.length; i++) { args.push(arguments[i]); }

    return implementation.apply(this, args);
  };

  return renameFunction(methodName, implementation, argNames);
}

function ShadowAPI(apiName, methods, methodBuilder) {
  var cons = new Function ();

  cons = renameFunction(apiName, cons, []);

  cons.prototype = {
  };

  for (var methodName in methods) {
    cons.prototype[methodName] = new ShadowMethod(methodName, methods[methodName], methodBuilder(apiName, methodName, methods[methodName]));
  }

  return cons;
}

function getFunctionArgumentNames(fn) {
  if (typeof(fn) === 'function') fn = fn.toString();

  var argNames = [],
      fnText = fn.replace(STRIP_COMMENTS, ''),
      argDecl = fnText.match(FN_ARGS),
      splitted = argDecl[2].split(FN_ARG_SPLIT);

  for (var i = 0; i < splitted.length; i++) {
    var arg = splitted[i];

    arg.replace(FN_ARG, function(all, underscore, name){
      argNames.push(name);
    });
  }

  return argNames;
}

function getFunctionBody(code) {
  if (typeof(code) === 'function') code = code.toString();

  var parts = require('function-body-regex').exec(code);

  return parts ? parts[1] || '' : '';
}

function getFunctionName(code) {
  return code.name;
}


module.exports = {
  getFunctionName: getFunctionName,
  getFunctionBody: getFunctionBody,
  getFunctionArgumentNames: getFunctionArgumentNames,
  createApi: ShadowAPI,
  createMethod: ShadowMethod,
  renameFunction: renameFunction
}
